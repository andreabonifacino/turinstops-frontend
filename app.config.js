(function(){
  'use strict';
  angular
    .module('turinstopsapp')
    .config(routerConfig)

  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider){
    $urlRouterProvider.otherwise("/login");

    // Now set up the states
    $stateProvider
      .state('login', {
        url: "/login",
        templateUrl: "app/login/login.html",
        controller: "loginController",
        controllerAs: "loginCtrl"
      })
      .state('stops', {
        url: "/stops",
        templateUrl: "app/stops/stops.html",
        controller: "stopsController",
        controllerAs: "stopsCtrl"
      })
  }
})();
