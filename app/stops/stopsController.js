(function(){
  'use strict';
  angular
    .module('turinstopsapp')
    .controller("stopsController",  stopsController);

  function stopsController ($state, $log, turinStopsStopsApi, NgMap){
    var vm = this;
    vm.stops = [];

    // Maps vars and functions
    vm.selectedCenter = null;
    vm.markers = [];
    vm.selectedMarkers = [];
    //vm.markerSelected = markerSelected;
    vm.showDetailOnMap = showDetailOnMap;
    //vm.hideDetailOnMap = hideDetailOnMap;
    //vm.onProductEnter = onProductEnter;
    //vm.onProductLeave = onProductLeave;
    //vm.placeChanged = placeChanged;
    vm.zoom = 15;

    init();

    function init(){
      var user = JSON.parse(localStorage.getItem("user"));
      if (user == null || user == undefined){
        window.location = "#/login";
        return;
      }
      getStops();
    }
    function getStops(){
      var user = JSON.parse(localStorage.getItem("user"));
      turinStopsStopsApi.stops(user.auth, getAllStopsSuccess, getAllStopsError);

      function getAllStopsSuccess(resp){
        $log.info(resp.data);
        vm.stops = resp.data;
        updateMarkers();
        vm.showMap = true;
        NgMap.getMap('stops-map').then(function(map) {
          vm.map = map;
          vm.map.setCenter({
            lat: 45.069830,
            lng: 7.686935
          });
        });
      }
      function getAllStopsError(error){
        $log.error(error);
      }
    }

    function updateMarkers(){
      vm.markers = [];
      for (var index in vm.stops){
        var stop = vm.stops[index];
        vm.markers.push({
          position: [stop.lat, stop.lng],
          stop: stop
        })
      }
    }

    function showDetailOnMap(e, stop) {
      var user = JSON.parse(localStorage.getItem("user"));
      turinStopsStopsApi.departures(stop.id, user.auth, departuresSuccess, departuresError);

      function departuresSuccess(resp){
        $log.info(resp);
        vm.stopInInfoWindow = stop;
        vm.stopInInfoWindow.departures = resp.data;
        vm.map.showInfoWindow('map-infowindow', stop.id);
      }

      function departuresError(error){
        $log.error(error);
      }
    };
  };
})();
