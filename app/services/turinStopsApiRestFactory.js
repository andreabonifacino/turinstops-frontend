(function(){
  'use strict';
  angular
    .module('turinstopsapp')
    .factory('turinStopsApi', turinStopsApi);

  function turinStopsApi($rootScope, $log, $http, $base64){
    var service = {
      turinStopsApiRestCall: turinStopsApiRestCall
    }

    return service;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function turinStopsApiRestCall (url, method, auth, contentType, transformRequest, headersExtra, data, callbackSuccess, callbackError) {
      var headers = {
        'Content-Type': contentType,
        'Accept': 'application/json'
      };
      if (auth) {
        var authorization = 'Basic ' + $base64.encode(auth.username + ':' + auth.password);
        headers['Authorization'] = authorization;
      }
      angular.extend(headers, headersExtra);
      return (
        $http({
          transformRequest: transformRequest,
          method: method,
          url: url,
          data: data,
          headers: headers
        })
        // success
        .then(
          function(response) {
            var resultError = false;
            try {
              resultError = response.data['result'].toLowerCase() == "failure";
            }catch(e){}
            if (response['status'] == 200 && !resultError) {
              callbackSuccess(response);
              $log.debug (response);
            } else {
              if (callbackError) callbackError(response);
              $log.error (response);
            }
          }, //error
          function(response) {
            if (callbackError) callbackError(response);
            $log.error (response);
          }
        )
      )
    }

    // function per fare ricerche paginate
    function paginationSearch(url, page, perPage, callbackThen){
      return $http.get(url,{
        params: {
          page: page,
          perPage: perPage
        }
      })
      .then(callbackThen)
    }
  }
})();
