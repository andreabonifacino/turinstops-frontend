(function(){
  'use strict';
  angular
    .module('turinstopsapp')
    .factory('turinStopsUserApi', [
      'turinStopsApi',
      '$rootScope',
      '$log',
      turinStopsUserApi
    ]);

  function turinStopsUserApi(turinStopsApi, $rootScope, $log){
    var service = {
      login : userLogin
    };

    return service;

    ////////////////////////////////////////////////////////////////////////////

    function userLogin(auth, callbackSuccess, callbackError){
      var url = $rootScope.turinstopsHost + '/user/login';
      turinStopsApi.turinStopsApiRestCall(url, 'GET', auth, undefined, null, null, null, callbackSuccess, callbackError);
    }
  }
})();
