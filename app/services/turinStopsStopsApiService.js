(function(){
  'use strict';
  angular
    .module('turinstopsapp')
    .factory('turinStopsStopsApi', [
      'turinStopsApi',
      '$rootScope',
      '$log',
      turinStopsStopsApi
    ]);

  function turinStopsStopsApi(turinStopsApi, $rootScope, $log){
    var service = {
      stops : getAllStops,
      departures: getNextDepartures
    };

    return service;

    ////////////////////////////////////////////////////////////////////////////

    function getAllStops(auth, callbackSuccess, callbackError){
      var url = $rootScope.turinstopsHost + '/stops';
      turinStopsApi.turinStopsApiRestCall(url, 'GET', auth, undefined, null, null, null, callbackSuccess, callbackError);
    }

    function getNextDepartures(stop, auth, callbackSuccess, callbackError){

      var url = $rootScope.turinstopsHost + '/stops/' + stop + '/departures';
      turinStopsApi.turinStopsApiRestCall(url, 'GET', auth, undefined, null, null, null, callbackSuccess, callbackError);
    }
  }
})();
