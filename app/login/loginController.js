(function(){
  'use strict';
  angular
    .module('turinstopsapp')
    .controller("loginController",  loginController);

  function loginController ($state, $log, turinStopsUserApi){
    var vm = this;
    vm.email = "";
    vm.password = "";
    vm.login = login;

    function login(){
      var auth = {
        "username": vm.email,
        "password": vm.password
      }
      turinStopsUserApi.login(auth, userLoginSuccess, userLoginError);

      function userLoginSuccess(resp){
        $log.info(resp.data);
        var user = resp.data;
        user.auth = auth;
        localStorage.setItem("user", JSON.stringify(user));
        window.location = "#/stops";
      }
      function userLoginError(error){
		alert("Login error \n", error);
        $log.error(error);
      }
    }
  };
})();
