(function(){
  'use strict';
  angular
    .module("turinstopsapp", ['ui.router', 'base64', 'ngMap'])
    .run(turinstopsRun);

    function turinstopsRun ($rootScope){
      $rootScope.turinstopsHost = window.location.protocol + "//" + document.location.hostname + ":" + 1234;
    }
})();
